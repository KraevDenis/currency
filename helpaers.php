<?php
if (!function_exists('config')) {

    function config($key)
    {
        return include __DIR__."/config/$key.php";
    }

}