<?php
namespace Controllers;

require './vendor/autoload.php';

use Carbon\Carbon;
use Carbon\CarbonPeriod;
use PDO;
use UseCases\CurrencyService;

class ApiController
{
    private string $requestMethod;
    private array $params = [];
    private string $authToken = '73a771a9ab8a5c373d3ae240a6350db6';
    private string $cbrApiUrl = 'http://www.cbr.ru/scripts/';
    private PDO $dbh;

    public function __construct($dbh)
    {
        $this->dbh = $dbh;
        $this->requestMethod = $_SERVER["REQUEST_METHOD"];
    }

    private function list()
    {
        if($this->requestMethod !== 'GET') {
            $this->response(['message' => 'Method not allowed'], 405);
        }

        $currencyService = new CurrencyService($this->dbh);

        $this->response([
            'list' => $currencyService->list($this->params),
            'filter' => [
                'valute_id' => $currencyService->filterValuteID(),
            ],
        ], 200);
    }

    private function updateData()
    {
        if($this->requestMethod !== 'POST') {
            $this->response(['message' => 'Method not allowed'], 405);
        }

        $currencyService = new CurrencyService($this->dbh);

        $period = CarbonPeriod::create(Carbon::now()->copy()->subDays(30), Carbon::now());

        $res = [];
        $this->dbh->beginTransaction();
        try {
            $currencyService->clear();

            foreach($period as $date) {
                $string = file_get_contents($this->cbrApiUrl.'/XML_daily.asp?date_req='.$date->format('d/m/Y'));
                $xml = simplexml_load_string($string);

                if(count($xml->Valute) > 0) {
                    foreach($xml->Valute as $key => $valute) {
                        $currencyService->create([
                            'valuteID' => (string)$valute->attributes()['ID'],
                            'numCode' => (int)$valute->NumCode,
                            'charCode' => (string)$valute->CharCode,
                            'name' => (string)$valute->Name,
                            'value' => (string)$valute->Value,
                            'date' => (string)$date->format('Y-m-d'),
                        ]);
                    }
                }
            }
        } catch(\Exception $e) {
            $this->dbh->rollBack();
            $this->response(['message' => $e->getMessage()], 500);
        }

        $this->dbh->commit();
        $this->response(['message' => $res], 200);
    }

    public function processRequest()
    {
        $this->parseParams();
        if(!isset($this->params['auth_token']) || $this->params['auth_token'] != $this->authToken) {
            $this->response(['message' => 'Unauthorised'], 401);
        }

        if(!isset($this->params['method']) || !method_exists(self::class, $this->params['method'])) {
            $this->response(['message' => 'Api method does not exist'], 500);
        }

        $this->{$this->params['method']}();
    }

    private function parseParams()
    {
        $this->params = json_decode(file_get_contents('php://input'), true)?:[];
        $this->params = array_merge($_POST, $this->params)?:[];
        $this->params = array_merge($_GET, $this->params);
    }

    private function response(array $data, $code)
    {
        header('Content-Type: application/json');
        http_response_code($code);
        echo json_encode($data);
        die();
    }
}