<?php
require_once __DIR__.'/simple_auth/auth.php';

if (isset($_POST['login']) && isset($_POST['password'])){
    login(trim($_POST['login']), trim($_POST['password']));
}

if(isAuth()) {
    header("Location: /");
    die();
}
?>

<!DOCTYPE html>
<html>
<head>
    <title>Курсы валют | Авторизация</title>
    <meta charset="utf-8">
    <link rel="stylesheet" href="/build/app.css">
</head>
<body class="page-auth">
<form method="POST" class="form-signin rounded border border-light p-5" action="">
    <h1 class="h3 mb-3 font-weight-normal text-center">Авторизация</h1>
    <div class="mb-3">
        <input id="email" type="text" class="form-control" placeholder="Логин" name="login" value="" autofocus>
    </div>
    <div class="mb-3">
        <input id="password" type="password" class="form-control" placeholder="Пароль" name="password">
    </div>
    <div class="row">
        <div class="col-md-12">
            <button type="submit" class="btn btn-lg btn-primary btn-block">Войти</button>
        </div>
    </div>
</form>
<script src="/build/app.js"></script>
</body>
</html>
