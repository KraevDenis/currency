<?php
use GuzzleHttp\Client as Guzzle;
use GuzzleHttp\Exception\ClientException;

require_once __DIR__.'/simple_auth/auth.php';
require './vendor/autoload.php';

if(!isAuth()) {
    header("Location: /login.php");
    die();
}

$client = new Guzzle(['base_uri' => 'http://'.$_SERVER['SERVER_NAME'].'/']);

try {
    $response = $client->request('POST', 'api.php', [
        'headers' => [
            'Content-type' => 'application/json',
            'Accept' => 'application/json'
        ],
        'body' => json_encode([
            'method' => 'updateData',
            'auth_token' => '73a771a9ab8a5c373d3ae240a6350db6',
        ])
    ]);

    $data = json_decode((string)$response->getBody(), true);

    header("Location: /");
    die();
} catch (\Exception $e) {
    /** @var ClientException $e */
    if(!empty($e->getResponse())) {
        $data = json_decode($e->getResponse()->getBody()->read(1000), true);
        if (is_array($data) && !empty($data) && json_last_error() === JSON_ERROR_NONE && isset($data['message'])) {
            echo $data['message'];
        } else {
            echo 'Ошибка 02';
        }
    } else {
        echo $e->getMessage();
    }
}
