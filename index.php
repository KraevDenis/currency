<?php
use Carbon\Carbon;
use GuzzleHttp\Client as Guzzle;
use GuzzleHttp\Exception\ClientException;

require_once __DIR__.'/simple_auth/auth.php';

require './vendor/autoload.php';

if(isset($_GET['unauth'])) {
    unAuth();
}

if(!isAuth()) {
    header("Location: /login.php");
    die();
}

$dates = [];
$currencies = [];

$client = new Guzzle(['base_uri' => 'http://'.$_SERVER['SERVER_NAME'].'/']);

try {
    $response = $client->request('GET', 'api.php', [
        'headers' => [
            'Content-type' => 'application/json',
            'Accept' => 'application/json'
        ],
        'body' => json_encode([
            'method' => 'list',
            'valuteId' => $_GET['valuteId']??null,
            'date' => $_GET['date']??null,
            'auth_token' => '73a771a9ab8a5c373d3ae240a6350db6',
        ])
    ]);

    $currencies = json_decode((string)$response->getBody(), true);
} catch (\Exception $e) {
    /** @var ClientException $e */
    if(!empty($e->getResponse())) {
        $data = json_decode($e->getResponse()->getBody()->read(1000), true);
        if (is_array($data) && !empty($data) && json_last_error() === JSON_ERROR_NONE && isset($data['message'])) {
            echo $data['message'];
        } else {
            echo 'Ошибка 02';
        }
    } else {
        echo $e->getMessage();
    }
}
?>

<!DOCTYPE html>
<html>
<head>
    <title>Курсы валют</title>
    <meta charset="utf-8">
    <link rel="stylesheet" href="/build/app.css">
</head>
<body>
<div class="container">
    <div class="mb-3 d-flex">
        <a href="update.php" class="btn btn-info">Обновить базу</a>
        <a href="index.php?unauth" class="btn btn-danger ml-auto">Выйти</a>
    </div>

    <?php if(isset($currencies['filter'])) { ?>
        <form method="get" >
            <div class="mb-3 d-flex">
                <select name="valuteId" class="form-control">
                    <option value="" disabled <?php if(!isset($_GET['valuteId'])) { ?> selected <?php } ?>>Выберите валюту</option>
                    <?php foreach($currencies['filter']['valute_id'] as $valuteId) { ?>
                        <option value="<?=$valuteId['valuteID']?>" <?php if($valuteId['valuteID'] == $_GET['valuteId']) { ?> selected <?php } ?>><?=$valuteId['name']?></option>
                    <?php } ?>
                </select>
                <input class="form-control date-range ml-1" name="date" value="<?=$_GET['date']?>" placeholder="Выберите дату" autocomplete="off">
                <input type="submit" class="btn btn-primary ml-3" name="Ок">
                <?php if(isset($_GET['valuteId']) || isset($_GET['date'])) { ?>
                    <a href="/" class="btn btn-secondary ml-1">Очистить</a>
                <?php } ?>
            </div>
        </form>
    <?php } ?>

    <?php if(isset($currencies['list']) && !empty($currencies['list'])) { ?>
        <table class="table">
            <tr>
                <th>valuteID</th>
                <th>numCode</th>
                <th>charCode</th>
                <th>name</th>
                <th>value</th>
                <th>date</th>
            </tr>
            <?php foreach($currencies['list'] as $currency) { ?>
                <tr>
                    <td><?=$currency['valuteID']?></td>
                    <td><?=$currency['numCode']?></td>
                    <td><?=$currency['charCode']?></td>
                    <td><?=$currency['name']?></td>
                    <td><?=$currency['value']?></td>
                    <td><?=Carbon::parse($currency['date'])->format('Y-m-d')?></td>
                </tr>
            <?php } ?>
        </table>
    <?php } else { ?>
        <div class="text-center">Результаов нет</div>
    <?php } ?>
</div>

<script src="/build/app.js"></script>
</body>
</html>