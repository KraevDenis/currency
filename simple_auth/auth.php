<?php
session_start();
function login($login, $password){
    session_regenerate_id();
    if($login == 'test' && $password == 'test') {
        $_SESSION['auth'] = true;
        return true;
    }
    return false;
}

function isAuth() {
    if(isset($_SESSION['auth']) && $_SESSION['auth'] == true) {
        return true;
    }
    return false;
}

function unAuth() {
    unset($_SESSION['auth']);
    session_regenerate_id();
}
