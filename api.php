<?php
use Controllers\ApiController;

require './vendor/autoload.php';

$config = config('db');
try {
    $dbh = new PDO("mysql:host=".$config['host'].";dbname=".$config['name']."", $config['username'], $config['password']);
    $dbh->exec('SET NAMES '.$config['charset']);
}
catch(PDOException $e) {
    exit($e->getMessage());
}

$controller = new ApiController($dbh);
$controller->processRequest();