<?php
namespace UseCases;

use PDO;

class CurrencyService
{
    private PDO $dbh;

    public function __construct($dbh)
    {
        $this->dbh = $dbh;
    }

    public function list($params)
    {
        $where = '';
        if(isset($params['date']) && !empty($date = $params['date'])) {
            $date = explode(' - ', $date);
            if(isset($date[0])) {
                if(!empty($where)) {$where .= ' AND ';}
                $where .= "date >= '$date[0]'";
            }
            if(isset($date[1])) {
                if(!empty($where)) {$where .= ' AND ';}
                $where .= "date <= '$date[1]'";
            }
        }

        if(isset($params['valuteId']) && !empty($valuteId = $params['valuteId'])) {
            if(!empty($where)) {$where .= ' AND ';}
            $where .= "valuteId = '$valuteId'";
        }

        if(!empty($where)) {
            $where = "WHERE $where";
            $sql = "SELECT * FROM currency $where ORDER BY date DESC";
            return $this->dbh->query($sql)->fetchAll(PDO::FETCH_ASSOC);
        }
        return null;
    }

    public function filterValuteID()
    {
        $sql = "SELECT valuteID, name FROM currency GROUP BY valuteID, name";
        return $this->dbh->query($sql)->fetchAll(PDO::FETCH_ASSOC);
    }

    public function create($params)
    {
        $sql = "INSERT INTO currency (
            valuteID,
            numCode,
            charCode,
            name,
            value,
            date
        ) VALUES (
            :valuteID,
            :numCode,
            :charCode,
            :name,
            :value,
            :date
        )";

        $stmt = $this->dbh->prepare($sql);
        $stmt->bindValue(":valuteID", $params['valuteID'], PDO::PARAM_STR);
        $stmt->bindValue(":numCode", $params['numCode'], PDO::PARAM_INT);
        $stmt->bindValue(":charCode", $params['charCode'], PDO::PARAM_STR);
        $stmt->bindValue(":name", $params['name'], PDO::PARAM_STR);
        $stmt->bindValue(":value", $params['value'], PDO::PARAM_STR);
        $stmt->bindValue(":date", $params['date'], PDO::PARAM_STR);
        $stmt->execute();
    }

    public function clear()
    {
        $this->dbh->prepare("DELETE FROM currency")->execute();
    }
}