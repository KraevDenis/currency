import "../../node_modules/bootstrap/scss/bootstrap.scss";
import "../sass/lib/daterangepicker.sass";

import "../sass/app.sass";

try {
    window.$ = window.jQuery = require('jquery');
    require('bootstrap-daterangepicker');
} catch (e) {}